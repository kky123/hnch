package com.ky.account.until;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class MD5Util {
	
	
	/**
	 * 只能通过字符串获取到对应的md5值，不能通过md5值获取到原始字符串
	 * 通常用来进行密码加密
	 * @param string
	 * @return
	 */
    public static String getMd5(String string) {
        try {
        	MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bs = md5.digest(string.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder(40);
            for (byte x : bs) {
                if ((x & 0xff) >> 4 == 0) {
                    sb.append("0").append(Integer.toHexString(x & 0xff));
                } else {
                    sb.append(Integer.toHexString(x & 0xff));
                }
            }
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 在任何时间任何地点获取到的UUID都不一样
     * @return
     */
    public static String getUUID() {
    	return UUID.randomUUID().toString().replaceAll("-","");
    }

}
