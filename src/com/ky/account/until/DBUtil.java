package com.ky.account.until;

import java.sql.*;

public class DBUtil {


	private static final String URL="jdbc:mysql://47.101.216.31:3306/hnch?useUnicode=true&characterEncoding=GBK";
	private static final String USER="hnch";
	private static final String PASSWORD="hnch";
	private static Connection conn=null;
	static{
		try {
//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
//创建连接
			conn=DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	public static Connection getConnection(){

		return conn;
	}


}