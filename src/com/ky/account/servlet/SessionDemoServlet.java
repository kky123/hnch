package com.ky.account.servlet;/*
   @author:ky 
   @qq:564350997
   @company risen
   @create  2020-02-12 20:29
   
*/

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionDemoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //获取session
        HttpSession session = req.getSession();
        session.setAttribute("name","张三");
        Object obj = session.getAttribute("name");

        String cmd = req.getParameter("cmd");
        if(cmd == null)
            return;
        if(cmd.equals("zhuanfa"))
        //转发
        req.getRequestDispatcher("/index.jsp").forward(req,resp);
        else if(cmd.equals("chongdingxiang"))
        //重定向
        resp.sendRedirect("/account/index.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
