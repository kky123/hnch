package com.ky.account.servlet;

import com.ky.account.entity.Account;
import com.ky.account.service.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.List;


/*
   @author:ky 
   @qq:564350997
   @company risen
   @create  2020-02-11 12:22
   
*/

public class AccountServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        AccountService accountService = new AccountService();
      /*  accountService.getAllUserInfo();

        Account account = new Account();
        accountService.addAccount(account);*/
        String cmd = request.getParameter("cmd");
        if (cmd == null) {
            System.out.println("请求参数错误");
            return;
        }
        if (cmd.equals("accountList"))
            accountService.getAllUserInfo();
            //执行查询所有用户信息。。
        else if (cmd.equals("addAccount")) {
            //注册
            Account account = new Account();
            String phone = request.getParameter("phone");
            String sex = request.getParameter("sex");
            String age = request.getParameter("age");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            account.setAge(Integer.valueOf(age));
            account.setSex(Integer.valueOf(sex));
            account.setUsername(username);
            account.setPassword(password);
            account.setName(name);
            account.setPhone(phone);
            //执行添加帐号
            if (accountService.addAccount(account)) {
                request.setAttribute("result", "添加成功");
                request.getRequestDispatcher("/result.jsp").forward(request, response);

            } else {
                request.setAttribute("result", "添加失败，帐号已经存在");
                request.getRequestDispatcher("/result.jsp").forward(request, response);
                ;
            }
        } else if (cmd.equals("login")) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            if (username == null||"用户名".equals(username)) {
                session.setAttribute("result", "输入帐号为空");
                response.sendRedirect("/account/error.jsp");
                return;
            }
            if (password == null||"password".equals(password)) {
                session.setAttribute("result", "输入密码为空");
                response.sendRedirect("/account/error.jsp");
                return;
            }
            Account account = accountService.login(username, password);
            if (account == null) {
                session.setAttribute("result", "帐号或者密码错误");
                response.sendRedirect("/account/error.jsp");
            } else {
                session.setAttribute("user", account);
                List<Account> accounts = accountService.getAllUserInfo();
                session.setAttribute("accounts",accounts);
                response.sendRedirect("/account/accountList.jsp");
            }

        }

    }
}


