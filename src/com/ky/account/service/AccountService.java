package com.ky.account.service;/*
   @author:ky 
   @qq:564350997
   @company risen
   @create  2020-02-11 23:08
   
*/

import com.ky.account.dao.AccountDao;
import com.ky.account.entity.Account;
import com.ky.account.until.MD5Util;


import java.util.List;

public class AccountService {


    public List getAllUserInfo() {
        AccountDao accountDao = new AccountDao();
        List<Account> allAccountInfo = accountDao.getAllAccountInfo();
        allAccountInfo.forEach(account ->{
            System.out.println(account.getAge());
            System.out.println(account.getPhone());
            System.out.println(account.getName());
            System.out.println(account.getCreateTime());
            System.out.println("====================");
        });
        return allAccountInfo;

    }

    public boolean addAccount(Account account) {
        AccountDao accountDao = new AccountDao();
        String username = account.getUsername();
        if (accountDao.isExits(username)) {
            System.out.println("帐号已经存在");
            return false;
        }
        String password = MD5Util.getMd5(account.getPassword());
        boolean save = accountDao.addAccount(account.getUsername(), password, account.getName(), account.getSex(), account.getAge(), account.getPhone());
        if (save) {
            System.out.println("帐号添加成功");
            return true;
        } else {
            System.out.println("帐号添加失败");
            return false;
        }
    }
    //登录
    public Account login(String username,String password){
        password = MD5Util.getMd5(password);
        AccountDao accountDao = new AccountDao();
        return accountDao.login(username,password);
    }
}
