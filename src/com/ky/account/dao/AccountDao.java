package com.ky.account.dao;/*
   @author:ky 
   @qq:564350997
   @company risen
   @create  2020-02-11 12:55
   
*/

import com.ky.account.entity.Account;
import com.ky.account.until.DBUtil;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AccountDao {

    public List<Account> getAllAccountInfo() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT name,sex,create_time FROM account ;" ;
        List<Account> list = new ArrayList<>();
        try {
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                String name = rs.getString("name");
                Integer sex = rs.getInt("sex");
                Timestamp createTime = rs.getTimestamp("create_time");
                Account account=new Account();
                account.setName(name);
                account.setSex(sex);
                account.setCreateTime(createTime.toLocalDateTime());
                list.add(account);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public boolean addAccount(String username,String password,String name,Integer sex,Integer age,String phone){

        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO account(username,password,name,sex,age,phone) VALUES (?,?,?,?,?,?);";
        try {
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            ps.setString(3,name);
            ps.setInt(4,sex);
            ps.setInt(5,age);
            ps.setString(6,phone);
            int count = ps.executeUpdate();
            if(count == 1)
                return true;
            return false;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    //判断帐号是否存在，，，
    //存在返回true，不存在返回 false
    public boolean isExits(String username){
        if(username == null)
            System.out.println("帐号为空");
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from account where username = ?";
        try {
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            rs = ps.executeQuery();
            while (rs.next()){
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    //登录
    public Account login(String username,String password){

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select username,name,age,sex,phone,create_time from account where username=? and password=?";
        try {
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs = ps.executeQuery();
            while (rs.next()){
                Account account = new Account();
                int age = rs.getInt("age");
                int sex = rs.getInt("sex");
                String name = rs.getString("name");
                String usernamex = rs.getString("username");
                String phone = rs.getString("phone");
                LocalDateTime createTime = rs.getTimestamp("create_time").toLocalDateTime();
                account.setAge(age);
                account.setName(name);
                account.setSex(sex);
                account.setCreateTime(createTime);
                account.setPhone(phone);
                account.setUsername(username);
                return  account;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
